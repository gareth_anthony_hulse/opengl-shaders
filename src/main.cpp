#include <GLFW/glfw3.h>
#include <iostream>
#include <unistd.h>
#include <cmath>
#include <GLES3/gl32.h>

/* Vetex shader source. */
const GLchar
*vertex150 = R"END(
#version 150
attribute vec4 in_color;
attribute vec4 in_position;
uniform mat4 matrix;
varying vec4 out_color;
void
 main()
{
  out_color = in_color;
  gl_Position = in_position * matrix;
}
)END";

/*Fragment shader source. */
const GLchar
*raster150 = R"END(
#version 150
varying vec4 out_color;
void 
main ()
{
  gl_FragColor = out_color;
}
)END";

int
main ()
{  
  GLFWwindow *window;
  if (!glfwInit ())
    {
      std::cerr << "GLFW failed to initalise!" << std::endl;
      exit (EXIT_FAILURE);
    }
  
  GLFWmonitor* monitor = glfwGetPrimaryMonitor ();
  const GLFWvidmode* mode = glfwGetVideoMode (monitor);
  glfwWindowHint (GLFW_RED_BITS, mode->redBits);
  glfwWindowHint (GLFW_GREEN_BITS, mode->greenBits);
  glfwWindowHint (GLFW_BLUE_BITS, mode->blueBits);
  glfwWindowHint (GLFW_REFRESH_RATE, mode->refreshRate);
  glfwWindowHint (GLFW_SCALE_TO_MONITOR, GLFW_TRUE);

  window = glfwCreateWindow (mode->width, mode->height, PACKAGE, monitor, nullptr);

  if (!window)
    {
      std::cerr << "Failed to create window!" << std::endl;
      glfwTerminate ();
      exit (EXIT_FAILURE);
    }
  
  glfwMakeContextCurrent (window);
  
  std::cout << "Checking OpenGL version: ";
  const unsigned char * msg;
  msg = glGetString(GL_VERSION);
  std::cout << msg << "\nShader language version: ";
  msg = glGetString(GL_SHADING_LANGUAGE_VERSION);
  std::cout << msg << "\n";

  GLint compliation_status; /* Make sure compliation is successful. */

  /* Vertex shader. */
  GLuint vertex_shader = glCreateShader (GL_VERTEX_SHADER);
  glShaderSource (vertex_shader, 1, &vertex150 ,0);
  glCompileShader (vertex_shader);
  glGetShaderiv (vertex_shader, GL_COMPILE_STATUS, &compliation_status);
  if (compliation_status == GL_FALSE)
    {
      GLchar message[256];
      glGetShaderInfoLog (vertex_shader, sizeof (message), 0, &message[0]);
      std::cerr << message << std::endl;
      exit (EXIT_FAILURE);
    }

  /* Fragment shader. */
  GLuint fragment_shader = glCreateShader (GL_FRAGMENT_SHADER);
  glShaderSource (fragment_shader, 1, &raster150, 0);
  glCompileShader (fragment_shader);
  glGetShaderiv (vertex_shader, GL_COMPILE_STATUS, &compliation_status);
  if (compliation_status == GL_FALSE)
    {
      GLchar message[256];
      glGetShaderInfoLog (fragment_shader, sizeof (message), 0, &message[0]);
      std::cerr << message << std::endl;
      exit (EXIT_FAILURE);
    }

  /* Shader program. */
  GLint link_status;
  GLuint shader_program = glCreateProgram ();
  glAttachShader (shader_program, vertex_shader);
  glAttachShader (shader_program, fragment_shader);
  glLinkProgram (shader_program);
  
  glGetProgramiv (shader_program, GL_LINK_STATUS, &link_status);
  if (compliation_status == GL_FALSE)
    {
      GLchar message[256];
      glGetShaderInfoLog (shader_program, sizeof (message), 0, &message[0]);
      std::cerr << message << std::endl;
      exit (EXIT_FAILURE);
    }

  glUseProgram (shader_program);

  /* VBOs. */
  GLfloat
    vertices[] =
    {
     -1, -1, 0,
     -1,  1, 0,
     1, -1, 0,
     1, -1, 0,
     -1,  1, 0,
     1,  1, 0
    },
    colors[] =
    {
     1, 0, 0,
     0, 1, 0,
     0, 0, 1,
     1, 1, 0,
     1, 0, 1,
     0, 1, 1
    };

  /* VBO setup. */
  GLuint vertex_buffer;
  glGenBuffers (1, &vertex_buffer);
  glBindBuffer (GL_ARRAY_BUFFER, vertex_buffer);
  glBufferData (GL_ARRAY_BUFFER, sizeof (vertices), vertices, GL_STATIC_DRAW);
  
  GLuint colors_buffer;
  glGenBuffers (1, &colors_buffer);
  glBindBuffer (GL_ARRAY_BUFFER, colors_buffer);
  glBufferData (GL_ARRAY_BUFFER, sizeof (colors), colors, GL_STATIC_DRAW);

  
  GLuint attrib_position = glGetAttribLocation (shader_program, "in_position");
  glEnableVertexAttribArray (attrib_position);
  glBindBuffer (GL_ARRAY_BUFFER, vertex_buffer);
  glVertexAttribPointer (attrib_position, 3, GL_FLOAT, GL_FALSE, 0, 0);

  GLuint attrib_color = glGetAttribLocation (shader_program, "in_color");
  glEnableVertexAttribArray (attrib_color);
  glBindBuffer (GL_ARRAY_BUFFER, colors_buffer);
  glVertexAttribPointer (attrib_color, 3, GL_FLOAT, GL_FALSE, 0, 0);

  /* Uniforms. */
  GLuint attribute_matrix = glGetUniformLocation (shader_program, "matrix");

  float alpha = 0;

  int
    width,
    height;

  float
    xscale,
    yscale;

  
  /* Render loop. */
  while (!glfwWindowShouldClose (window))
    {
      glfwSetInputMode (window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
      
      glClearColor (0, 0, 0, 0);
      glClear (GL_COLOR_BUFFER_BIT);

      /* Set viewport size to match the framebuffer. */
      glfwGetFramebufferSize (window, &width, &height);
      glfwSetWindowAspectRatio(window, width, height);
      glfwGetMonitorContentScale(monitor, &xscale, &yscale);
      glViewport (xscale, yscale, width, height);

      /* Spin the square. */
      float sa = 0.5 * sin (alpha);
      float ca = 0.5 * cos (alpha);
      alpha += 0.1;
      const GLfloat
	matrix[] =
	{
	 sa, -ca, 0, 0,
	 ca, sa, 0, 0,
	 0, 0, 1, 0,
	 0, 0, 0, 1
	};
      glUniformMatrix4fv (attribute_matrix, 1, GL_FALSE, matrix);

      /* Draw the sqaure. */
      glDrawArrays (GL_TRIANGLES, 0, 6);

      /* Draw buffer. */
      glfwSwapBuffers (window);
      glfwPollEvents ();
    }

  glfwTerminate ();
}
